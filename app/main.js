import React from 'react';

class Hello {
  render() {
    return (
      <div>
        <Hello2/>
      </div>
    )
  }
}
class Hello2 {
  render() {
    return <div>Hello3</div>
  }
}

React.render(
  <Hello />,
  document.getElementById('root')
);
