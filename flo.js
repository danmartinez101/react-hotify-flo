var flo = require('fb-flo'),
    path = require('path'),
    fs = require('fs'),
    exec = require('child_process').exec;

var server = flo(
  './app',
  {
    port: 8888,
    host: 'localhost',
    verbose: false,
    glob: [
      '**/*.js',
      '**/*.css'
    ]
  },
  function resolver(filepath, callback) {
    var build = "npm run build";
    exec(build, function (error) {
      if(error) console.log(error);
      callback({
        resourceURL: 'dist/bundle' + path.extname(filepath),
        // any string-ish value is acceptable. i.e. strings, Buffers etc.
        contents: fs.readFileSync('dist/bundle' + path.extname(filepath)).toString(),
        reload: false,
        update: function(_window, _resourceURL) {
          console.log("Resource " + _resourceURL + " has just been updated with new content");
          window.forceUpdateAll();
        }
      });
    });
  }
);
console.log('flo is running on localhost:8888');
